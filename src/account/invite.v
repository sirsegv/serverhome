module account

import rand

pub struct InviteCode {
pub:
	code string
}

pub fn (mut a Accounts) create_invite() !string {
	invite := InviteCode{rand.string(5)}

	sql a.db {
		insert invite into InviteCode
	} or { eprintln('Failed to add invite code to database: ${err.msg()}') }

	return invite.code
}