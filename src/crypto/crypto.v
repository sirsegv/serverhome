import crypto.aes

const encryptionkey = ''

fn encrypt_string(input string) string {
	if encryptionkey.len == 0 { return input }

	cipher := aes.new_cipher(encryptionkey.bytes())

	mut encrypted := []u8{len: aes.block_size}

	cipher.encrypt(mut encrypted, input.bytes())

	return encrypted.bytestr()
}

fn decrypt_string(input string) string {
	if encryptionkey.len == 0 { return input }

	cipher := aes.new_cipher(encryptionkey.bytes())

	mut decrypted := []u8{len: aes.block_size}

	cipher.decrypt(mut decrypted, input.bytes())

	return decrypted.bytestr()
}