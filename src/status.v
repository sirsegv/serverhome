import vweb
import os

const all_services = [
	Service{name: 'misskey', display_name: 'Misskey'}
	Service{name: 'dendrite', display_name: 'Matrix'}
	Service{name: 'jellyfin', display_name: 'JellyFin'}
	Service{name: 'minetest', display_name: 'Minetest'}
]

struct Service {
	display_name string
	name string
mut:
	status string
	color string
}

fn (mut s Service) update() {
	s.status = os.execute('systemctl is-active ${s.name}').output.trim_space()

	s.color = match s.status {
		'active' { '#0f0' }
		'inactive' { '#f00' }
		else { '#fff' }
	}
}

fn (mut s Server) status() vweb.Result {
	mut services := all_services.clone()
	
	for mut service in services {
		service.update()
	}

	return $vweb.html()
}