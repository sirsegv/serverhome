import vweb
import account

fn (mut s Server) check_token_cookie() bool {
	mut auth := false
	
	lock s.accounts {
		auth = s.accounts.check_token(s.get_cookie('token') or {return false})
	}

	return auth
}

// [middleware: check_token_cookie]
['/account']
fn (mut s Server) account() vweb.Result {
	mut user := account.User{}

	lock s.accounts {
		user = s.accounts.user_from_token(s.get_cookie('token') or {''}) or { return s.redirect('/signin') }
	}

	return s.text('Logged in as ${user.username}')
}

['/signin']
fn (mut s Server) signin() vweb.Result {
	if s.check_token_cookie() {
		return s.redirect('/account')
	}
	
	return $vweb.html()
}

['/logout']
fn (mut s Server) logout() vweb.Result {
	if token := s.get_cookie('token') {
		lock s.accounts {
			s.accounts.delete_token(token)
		}
	}

	return s.redirect('/signin')
}

['/signup'; post]
fn (mut s Server) signup() vweb.Result {
	user := account.User {
		username: s.form['username']
		displayname: s.form['displayname']
		password: s.form['password']
	}
	
	lock s.accounts {
		s.accounts.add_user(user) or {
			println('Error adding user')
			println(err)
			return s.server_error(500)
		}

		token := s.accounts.create_token(user) or {
			println('Error generating token')
			println(err)
			return s.server_error(500)
		}

		s.set_cookie(name: 'token', value: token)
	}

	return s.text('Welcome, ${user.username}')
}

['/login'; post]
fn (mut s Server) login() vweb.Result {
	login_username := s.form['username'] or {''}
	login_password := s.form['password'] or {''}

	if login_username == '' || login_password == '' {
		s.redirect('/account')
	}

	lock s.accounts {
		token := s.accounts.login(login_username, login_password) or { println('Error logging in') println(err) return s.server_error(500) }
		s.set_cookie(name: 'token', value: token)
	}

	return s.redirect('/account')
}